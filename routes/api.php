<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::get('/author', 'Articles\AddArticleController@show');
// Route::group(['middleware' => 'auth:api'], function () {
Route::get('/log', 'Auth\LoginController@isLogin');
Route::post('/addArticle', 'Articles\AddArticleController@addArticle');
Route::get('/articles', 'Articles\AddArticleController@showArticles');
Route::get('/article/{id}', 'Articles\AddArticleController@showArticle');
Route::put('/editArticle/{id}', 'Articles\AddArticleController@editArticle');
Route::delete('/deleteArticle/{id}', 'Articles\AddArticleController@deleteArticle');

Route::get('/home', 'Articles\HomePageController@articles');
Route::get('/home/{id}', 'Articles\HomePageController@singleArticle');


Route::post('/home/{id}', 'Comments\CommentController@reply');

Route::delete('/comment/{id}', 'Comments\CommentController@deleteComment');
// });

// Route::group(['middleware' => 'admin'], function () {
//   Route::get('/author', 'Articles\AddArticleController@show');
// });
