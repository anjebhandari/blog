<?php

namespace App\Model\Comments;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Comment extends Model
{
    protected $fillable = [
        'comment', 'user_id', 'article_id'
    ];

    public function article()
    {
        // return $this->hasOne(User::class, 'id', 'article_id');
        return $this->belongsTo(Article::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
