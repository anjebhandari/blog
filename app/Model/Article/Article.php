<?php

namespace App\Model\Article;

use App\User;
use App\Model\Comments\Comment;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'body', 'user_id', 'is_published'
    ];

    protected $pirmary = 'id';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
        // return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
