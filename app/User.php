<?php

namespace App;

use App\Model\Article\Article;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateToken()
    {
        // $this->api_token = Str::random(60);
        $this->api_token = bin2hex(openssl_random_pseudo_bytes(30));
        $this->save();

        return $this->api_token;
    }

    public function articles()
    {
        // return $this->hasMany(Article);
        // return $this->hasOne(User::class, 'user_id', 'id');
    }
}
