<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Admin extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if (Auth::check()) {
        $role = Auth::user()->role;

        if ($role == 'ADMIN')
            return $next($request);
        else
            return response("WTF???");
        // } else {
        //     return response("Not Logged in");
        // }
    }
}
