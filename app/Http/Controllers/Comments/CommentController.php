<?php

namespace App\Http\Controllers\Comments;

use Illuminate\Support\Facades\Auth;
use App\Model\Comments\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function reply(Request $request, $id)
    {
        $user = Auth::guard('api')->user();
        if ($user) {
            $reply = $request->all();
            $reply['article_id'] = $id;
            $reply['user_id'] = $user->id;
            $comment = Comment::create($reply);

            if ($comment)
                return response()->json($comment, 201);
            else
                return response()->json(['error' => 'not added']);

            // return response()->json($reply);
        }
    }

    public function deleteComment(Request $request, $id)
    {
        $user = Auth::guard('api')->user();
        if ($user) {
            $user_id = $user->id;
            $comment = Comment::findOrFail($id);
            if ($user_id == $comment->user_id) {
                $comment->delete();
                return response()->json([$id, $user_id], 200);
            }
        }
    }
}
