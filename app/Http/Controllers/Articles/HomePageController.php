<?php

namespace App\Http\Controllers\Articles;

use App\Model\Article\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Comments\Comment;

class HomePageController extends Controller
{
    public function articles()
    {
        $articles = Article::orderBy('created_at', 'desc')->with('comments')->with('user')->where('is_published', true)->take(10)->get();
        // $articles = Article::orderBy('created_at', 'desc')->with('comments.user')->with('user')->take(10)->get();
        return response()->json($articles, 200);
    }

    public function singleArticle($id)
    {
        if ($article = Article::where('id', $id)->with('comments.user')->with('user')->get()->first()) {
            return response()->json($article, 200);
        } else
            return response()->json(["data" => 'wtf??']);
    }
}
