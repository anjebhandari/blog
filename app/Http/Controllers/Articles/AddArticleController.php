<?php

namespace App\Http\Controllers\Articles;

use Carbon\Carbon;
use App\Model\Article\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddArticleController extends Controller
{
  public function showArticles()
  {
    $user = Auth::guard('api')->user();
    if ($user) {
      $role = $user->role;
      $id = $user->id;
      if ($role == "AUTHOR") {
        $articles = Article::where('user_id', $id)->orderBy('created_at', 'desc')->take(5)->get();
        return response()->json($articles, 200);
      } elseif ($role == "READER") { }
      return respones()->json("Not user type", 401);
    }
  }

  public function addArticle(Request $request)
  {
    $user = Auth::guard('api')->user();
    if ($user) {
      // $a = Auth::guard('api')->user()->role;
      $a = $user->role;

      if ($a == "ADMIN") {
        return response()->json(['ADMIN'], 200);
      } elseif ($a == "AUTHOR") {
        $articleData = $request->all();
        $articleData['user_id'] = $user->id;
        $article = Article::create($articleData);
        return response()->json($article, 201);
      } elseif ($a == "READER") {
        return response()->json(['READER'], 200);
      } else {
        return response()->json(['Unauthorized user'], 401);
      }
    } else {
      return response()->json(['not user'], 400);
    }
  }

  public function showArticle($id)
  {
    $user = Auth::guard('api')->user();
    if ($user) {
      $role = $user->role;
      $userId = $user->id;
      if ($role == "AUTHOR") {
        if ($article = Article::where('id', $id)->Where('user_id', $userId)->get())
          return response()->json($article, 200);
        else
          return resplone()->json("ArticleNot found");
      } elseif ($role == "READER") { }
      return respones()->json("Not user type", 401);
    }
  }

  public function editArticle(Request $request, Article $article, $id)
  {
    $result = Article::find($id);
    $result->id = $request->id;
    $result->title = $request->title;
    $result->body = $request->body;
    $result->is_published = $request->is_published;
    $result->updated_at = Carbon::now();
    if ($result->update())
      return response()->json($result);
    else
      return response()->json(['data' => 'Not edited'], 400);
  }

  public function deleteArticle(Request $request, $id)
  {
    $article = Article::findOrFail($id);
    $article->delete();

    return response()->json(['data' => 'deleted'], 204);
  }
}
