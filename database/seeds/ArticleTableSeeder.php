<?php

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Model\Article\Article;
use App\User;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 50; $i++) {
            $user_id = User::all()->pluck('id')->toArray();
            Article::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph(100, 3),
                'is_published' => $faker->randomElement([0, 1]),
                'user_id' => $faker->randomElement($user_id)
            ]);
        }
    }
}
