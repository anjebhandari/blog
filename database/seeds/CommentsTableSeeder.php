<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Model\Article\Article;
use App\User;
use App\Model\Comments\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 300; $i++) {
            $user_id = User::all()->pluck('id')->toArray();
            $article_id = Article::all()->pluck('id')->toArray();
            Comment::create([
                'comment' => $faker->sentence(30),
                'user_id' => $faker->randomElement($user_id),
                'article_id' => $faker->randomElement($article_id)
            ]);
        }
    }
}
