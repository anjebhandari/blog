<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'name' => $faker->name(),
                'email' => $faker->safeEmail(),
                'password' => Hash::make('123456789')
            ]);
        }
        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'ADMIN'
        ]);
        User::create([
            'name' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'ADMIN'
        ]);
        User::create([
            'name' => 'author',
            'email' => 'author@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'AUTHOR'
        ]);
        User::create([
            'name' => 'author2',
            'email' => 'author2@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'AUTHOR'
        ]);
        User::create([
            'name' => 'reader',
            'email' => 'reader@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'READER'
        ]);
        User::create([
            'name' => 'reader2',
            'email' => 'reader2@gmail.com',
            'password' => Hash::make('123456789'),
            'role' => 'READER'
        ]);
    }
}
